function Comparators(){
    var self = this;
    self.byName = function(person1,person2){
        return person1.firstName.localeCompare(person2.firstName);
    }
    self.bySurname = function(person1,person2){
        return person1.lastName.localeCompare(person2.lastName);
    }
    self.byAge = function(person1,person2){
        return person1.age - person2.age;
    }
    self.byId = function(person1,person2){
        return person1.id - person2.id;
    }
    self.byGender = function(person1,person2){
        return person1.gender.localeCompare(person2.gender);
    }
    self.byEmail = function(person1,person2){
        return person1.email.localeCompare(person2.email);
    }
    self.byIncome = function(person1,person2){
        return person1.income - person2.income;
    }

    self.byBirthDate = function(person1,person2){
        var datePerson1 = new Date(person1.birthsday);
        var datePerson2 = new Date(person2.birthsday);
        return datePerson1 - datePerson2;
    }
}
